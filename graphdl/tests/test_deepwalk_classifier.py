'''
Testing deepwalk_classifier
'''

import unittest
from scripts.deepwalk_classifier import read_group_memberships, deepwalk_scorer
import logging
import os
logger = logging.getLogger(__name__)

class TestDeepwalkClassifier(unittest.TestCase):
    '''
    Tests Deepwalk classifier with BlogCatalog dataset
    '''
    def setUp(self):
        self.dataset = 'BlogCatalog'
        self.data_dir = os.path.abspath(os.path.join(
                                    os.path.dirname(__file__),
                                    '../data/' + self.dataset + '-dataset/'
                                    ))

        self.embeddings_file = os.path.join(self.data_dir, 'blogcatalog.embeddings')
        self.memberships_file = os.path.join(self.data_dir, 'data/group-edges.csv')

    def test_read(self):
        logger.info("membership_file: {}".format(self.memberships_file))
        label_matrix = read_group_memberships(os.path.abspath(self.memberships_file))
        self.assertEqual(label_matrix.shape[0], 10312)
        self.assertEqual(label_matrix.shape[1], 39)
        self.assertEqual(label_matrix.nnz, 14476)

    def test_scorer(self):
        training_percents = [0.5]
        averages = ["samples"]
        number_shuffles = 10
        scores = deepwalk_scorer(self.embeddings_file, self.memberships_file,
                                training_percents, averages, number_shuffles)
        mean_F1_score = 0
        for i in range(number_shuffles):
            mean_F1_score = mean_F1_score + scores[0.5][i]["samples"] / number_shuffles
        logger.info("mean F1-score {}".format(mean_F1_score))
        self.assertAlmostEqual(mean_F1_score, 0.35, 2)

if __name__ == '__main__':
    unittest.main()
