# Simple tests

## GPU computation 
The files `check1.py` and `check2.py` check whether the GPU-based computation is working in theano.
See http://deeplearning.net/software/theano/tutorial/using_gpu.html for more details. 

Also given below is the `~/.theanorc` file 

     [global]   
     mode = FAST_RUN
     floatX = float32
     exception_verbosity = high
     device=gpu0

     [cuda]
     root=/usr/local/cuda-7.0

     [nvcc]
     fastmath = True



