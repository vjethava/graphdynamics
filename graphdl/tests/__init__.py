"""
This package contains the automated tests
"""
import logging
logging.basicConfig(format='%(asctime)s : %(module)s:%(lineno)d %(levelname)s : %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)
