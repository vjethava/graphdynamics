from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils.dot_utils import Grapher

class Model(object):

	def get_model(self, loss, optimizer, class_mode, nb_classes, input_shape):
		'''
		convolutional Neural Network with fully connected output layer
		'''

		input_depth = 1
		nb_filter = 32
		nb_row = 3
		nb_col = 3
		conv_to_dense = nb_filter * (input_shape[2]/2) * (input_shape[2]/2)

		model = Sequential()
		grapher = Grapher()

		model.add(Convolution2D(nb_filter = nb_filter,
								stack_size = input_depth,
								nb_row = nb_row,
								nb_col = nb_col,
								activation = 'relu',
								border_mode = 'full'))
		model.add(Convolution2D(nb_filter = nb_filter,
								stack_size =nb_filter,
								nb_row = nb_row,
								nb_col = nb_col,
								activation = 'relu'))
		model.add(MaxPooling2D(poolsize = (2, 2))) # Halves the image in each dimension
		model.add(Dropout(0.25))
		model.add(Flatten())
		model.add(Dense(input_dim = conv_to_dense,
						output_dim = 128,
						activation = 'relu'))
		model.add(Dropout(0.5))
		model.add(Dense(input_dim = 128,
						output_dim = nb_classes,
						activation = 'softmax'))

		grapher.plot(model, 'model.png')

		model.compile(loss=loss, optimizer=optimizer, class_mode=class_mode)
		return model


	def train(self, X_train, Y_train, batch_size, nb_epoch):
		self.model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch,
					show_accuracy=True, verbose=1, validation_split=0.1)

	def evaluate(self, name, X_test, Y_test):
		self.score = self.model.evaluate(X_test, Y_test, show_accuracy=True, verbose=1)
		with open("CNN Sampled Results.txt", 'a+') as f:
			f.write("Dataset: "+name+"\n")
			f.write("Loss on validation set: "+str(self.score[0])+"\n")
			f.write("Accuracy on validation set: "+str(self.score[1])+"\n")
			f.write("\n")
		
		print('Test Loss: ', self.score[0])
		print('Test Accuracy: ', self.score[1])

	def __init__(self, loss, optimizer, class_mode, nb_classes, input_shape):
		self.loss = loss
		self.optimizer = optimizer
		self.class_mode = class_mode
		self.nb_classes = nb_classes
		self.input_shape = input_shape
		self.model = self.get_model(loss, optimizer, class_mode, nb_classes, input_shape)

if __name__ == '__main__':


	# Convnet parameters
	batch_size = 128
	nb_epoch = 6
	nb_classes = 2
	class_mode = 'binary'
	optimizer = 'adadelta'
	loss = 'categorical_crossentropy'
	input_shape = (1706355, 1, 5, 5)

	# Building and training model
	model = Model(loss, optimizer, class_mode, nb_classes, input_shape)
	model.train(X_train, Y_train, batch_size, nb_epoch)
	model.evaluate(data, X_test, Y_test)