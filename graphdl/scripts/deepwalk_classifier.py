'''
Vinay Jethava <vjethava@gmail.com>
'''

import os
import sys
import random
from io import open
from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
import logging

from numpy import iterable
import warnings
import numpy
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
try:
    from itertools import izip
except: # for python 3.4
    from itertools import zip_longest as izip

from sklearn.metrics import f1_score
from scipy.io import loadmat
from sklearn.utils import shuffle as skshuffle

from collections import defaultdict
from gensim.models import Word2Vec
from scipy.sparse import csc_matrix


logger = logging.getLogger(__name__)
LOGFORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

__doc__ =   '''
            This function runs the 1-vs-Rest classification using liblinear
            based on an embedding obtained from DeepWalk
            '''

__author__ = 'Vinay Jethava'


class TopKRanker(OneVsRestClassifier):
    def predict(self, X, top_k_list):
        assert X.shape[0] == len(top_k_list)
        probs = numpy.asarray(super(TopKRanker, self).predict_proba(X))
        all_labels = []
        for i, k in enumerate(top_k_list):
            probs_ = probs[i, :]
            labels = self.classes_[probs_.argsort()[-k:]].tolist()
            all_labels.append(labels)
        return all_labels

def sparse2graph(x):
    G = defaultdict(lambda: set())
    cx = x.tocoo()
    for i,j,v in izip(cx.row, cx.col, cx.data):
        G[i].add(j)
    return {str(k): [str(x) for x in v] for k,v in G.iteritems()}



def main():
    parser = ArgumentParser(prog=None, usage=None, description=__doc__,
                            epilog=None, version=None, parents=[],
                            formatter_class=ArgumentDefaultsHelpFormatter,
                            prefix_chars='-', fromfile_prefix_chars=None,
                            argument_default=None, conflict_handler='resolve',
                            add_help=True)

    parser.add_argument('-e', '--embeddings_file', nargs='?', required=True,
                        help='embedding file for each node given by DeepWalk')

    parser.add_argument('-m', '--node_memberships',  nargs='?', required=True,
                        help='group labels for each node')

    # parser.add_argument('-g', '--group_list', nargs='?', required=False,
    #                     default=None,
    #                     help='''list of group IDs: if not specified,
    #                              inferred from node_memberships''')
    #
    # parser.add_argument('-n', '--node_list', nargs='?', required=False,
    #                     default=None,
    #                     help='''list of node IDs if not specified, inferred
    #                             from node_memberships''')

    parser.add_argument("-l", "--log_level", dest="log", default="INFO",
                        help="log verbosity level")

    parser.add_argument("--debug", dest="debug", action='store_true', default=False,
                        help="drop a debugger if an exception is raised.")


    parser.add_argument('-t', '--training_percent',  nargs='?', default=0.5,
                        type=float,
                        help='Fraction of data to use for training')

    parser.add_argument('-a', '--averaging', nargs='?', default='samples',
                        help=''' Which F1-score to use 'samples',
                                 'micro', 'macro', 'weighted'
                                 (default = 'samples')
                             '''
                        )

    parser.add_argument('-n', '--number_shuffles', nargs='?', default=1, type=int,
                        help='number of random training/testing pairs to create')
    args = parser.parse_args()
    numeric_level = getattr(logging, args.log.upper(), None)
    logging.basicConfig(format=LOGFORMAT)
    logger.setLevel(numeric_level)

    if args.debug:
        sys.excepthook = debug
    logger.info("averaging: {}".format(args.averaging))
    score = deepwalk_scorer(args.embeddings_file,
                    args.node_memberships,
                    args.training_percent,
                    [args.averaging],
                    args.number_shuffles
                    )
    F1_score = 0.0
    for i in range(args.number_shuffles):
        F1_score = F1_score + score[args.training_percent][i][args.averaging]
    F1_score = F1_score / args.number_shuffles

    logger.info("training_percent: {} averaging: {} num_shuffles: {} mean F1-score: {}".format(
                args.training_percent,
                [args.averaging],
                args.number_shuffles,
                F1_score
        ))
    return F1_score

def read_group_memberships(memberships_file):
    from scipy.sparse import csc_matrix
    node_list = dict()
    group_list = dict()

    num_nodes = 0
    num_groups = 0
    rowidx = []
    colidx = []
    data = []

    with open(memberships_file, 'r') as fid:
        for line in fid.readlines():
            (node_id, group_id) = map(int, line.strip().split(',')[:2])

            if group_id not in group_list:
                group_list[group_id] = num_groups
                num_groups = num_groups + 1

            if node_id not in node_list:
                node_list[node_id] = [] # use thiis
                num_nodes = num_nodes + 1

            node_list[node_id].append(group_id)

    for node_id in node_list:
        for group_id in set(node_list[node_id]):
            rowidx.append(node_id - 1)
            colidx.append(group_id - 1)
            data.append(1)

    logger.info("num_nodes: {} num_groups: {}".format(num_nodes, num_groups))
    labels_matrix = csc_matrix((data, (rowidx, colidx)), shape=(num_nodes, num_groups))
    return labels_matrix

def deepwalk_scorer(embeddings_file,
                    memberships_file,
                    training_percents=None,
                    averages=None,
                    number_shuffles=1):
    """
    Classification based on given embedding using different F1-score metrics
    """

    # How much data to keep for training
    if training_percents is None:
        training_percents = [0.1, 0.5, 0.9]

    # which F1-score to use
    if averages is None:
        averages = ["micro", "macro", "samples", "weighted"]



    model = Word2Vec.load_word2vec_format(embeddings_file, binary=False,
                                            norm_only=False)

    labels_matrix = read_group_memberships(memberships_file)

    # FIXED indexing here
    features_matrix = numpy.asarray(  [model[str(node)] for node in range(1, labels_matrix.shape[0] + 1 ) ] )

    # 2. Shuffle, to create train/test groups
    shuffles = []

    for x in range(number_shuffles):
      shuffles.append(skshuffle(features_matrix, labels_matrix))

    # 3. to score each train/test group
    all_results = defaultdict(list)

    if not iterable(training_percents):
        training_percents = [training_percents]

    # uncomment for all training percents
    #training_percents = numpy.asarray(range(1,10))*.1
    for train_percent in training_percents:
      for ii in range(number_shuffles):
        shuf = shuffles[ii]
        X, y = shuf

        training_size = int(train_percent * X.shape[0])

        X_train = X[:training_size, :]
        y_train_ = y[:training_size]

        y_train = [[] for x in range(y_train_.shape[0])]


        cy =  y_train_.tocoo()
        for i, j in izip(cy.row, cy.col):
            y_train[i].append(j)

        assert sum(len(l) for l in y_train) == y_train_.nnz

        X_test = X[training_size:, :]
        y_test_ = y[training_size:]

        y_test = [[] for x in range(y_test_.shape[0])]

        cy =  y_test_.tocoo()
        for i, j in izip(cy.row, cy.col):
            y_test[i].append(j)

        results = {}

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            clf = TopKRanker(LogisticRegression())
            clf.fit(X_train, y_train)

        # find out how many labels should be predicted
        top_k_list = [len(l) for l in y_test]
        preds = clf.predict(X_test, top_k_list)

        logger.debug("training: {} shuffle: {}/{}".format(averages, ii, number_shuffles))
        # FIXED warning messages here
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            for average in averages:
                results[average] = f1_score(y_test,  preds, average=average)

        all_results[train_percent].append(results)

    return all_results
    # print 'Results, using embeddings of dimensionality', X.shape[1]
    # print '-------------------'
    # for train_percent in sorted(all_results.keys()):
    #     print 'Train percent:', train_percent
    #     for x in all_results[train_percent]:
    #         print  x
    #     print '-------------------'


def convert_embedding_to_svmlight(embeddingFile, svmLightFile):
    '''
    This function converts embedding generated by DeepWalk to svmlight format
    '''
    with open(embeddingFile, 'r') as efid:
        logger.info('reading embedding file: %s', embeddingFile)
        (num_nodes, num_embeddings) = map(int, efid.readline().strip().split())
        logger.info('num_nodes: %d', num_nodes)
        logger.info('num_embeddings: %d', num_embeddings)
        lines = efid.readlines()
        assert(len(lines) == num_nodes) # should have an embedding for each node
        my_lines = dict()
        raise NotImplementedError()
        # for line in lines:
            #


if  __name__ == '__main__':
    main()
