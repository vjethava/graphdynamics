from __future__ import absolute_import
from __future__ import print_function

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils

import scipy.io as sio
import numpy as np
import numpy.random as rand
from numpy import ceil, log2, log, ones, ravel, reshape, ndarray, arange
from numpy.random import permutation, choice, shuffle
#np.random.seed(1337) # for reproducibility

'''
This script downloads the MUTAG dataset and samples m subgraphs from each
graph where m is the number given by eq(10) in Shervashidze et. al 2013.
A convolutional neural network is then trained on this dataset and then evaluated.
'''

class Model(object):

	def get_model(self, loss, optimizer, class_mode, nb_classes):
		'''
		convolutional Neural Network with fully connected output layer
		'''
		model = Sequential()

		model.add(Convolution2D(32, 1, 3, 3, border_mode='full'))
		model.add(Activation('relu'))
		model.add(Convolution2D(32, 32, 3, 3))
		model.add(Activation('relu'))
		model.add(MaxPooling2D(poolsize=(2, 2)))
		model.add(Dropout(0.25))

		model.add(Flatten())
		model.add(Dense(128, 128))
		model.add(Activation('relu'))
		model.add(Dropout(0.5))

		model.add(Dense(128, nb_classes))
		model.add(Activation('softmax'))

		model.compile(loss=loss, optimizer=optimizer, class_mode=class_mode)
		return model

	def train(self, X_train, Y_train, X_test, Y_test, batch_size, nb_epoch):
		self.model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch,
					show_accuracy=True, verbose=1, validation_data=(X_test, Y_test))

	def evaluate(self, X_test, Y_test):
		self.score = model.evaluate(X_test, Y_test, show_accuracy=True, verbose=0)
		print('Test score: ', score[0])
		print('Test Accuracy: ', score[1])

	def __init__(self, loss, optimizer, class_mode, nb_classes):
		self.loss = loss
		self.optimizer = optimizer
		self.class_mode = class_mode
		self.nb_classes = nb_classes
		self.model = self.get_model(loss, optimizer, class_mode, nb_classes)

def get_data(dataset):
	'''
	Loads .mat dataset containing graphs and labels
	'''
	mutag = sio.loadmat(dataset+'.mat')
	labels = mutag['l'+dataset.lower()]
	graphs = mutag['MUTAG'][0]
	adj_matrices = [graph[0] for graph in graphs]
	return [adj_matrices, labels]

def graph_sampler(matrix, k):
    '''
    Samples k rows and columns without replacement from input matrix.
    Returns subgraph of size k x k.
    '''
    n = matrix.shape[0]
    nodes = choice(n, k, replace=False)
    subgraph = ndarray(shape=(k,k))
    for i, i_node in enumerate(nodes):
        for j, j_node in enumerate(nodes):
            subgraph[i,j]=matrix[i_node,j_node]
    return subgraph

def data_generator(matrices, labels, k, a, epsilon, delta):
	'''
	Calls graph_sampler num_graphs * m times.
	Returns dataset of subgraphs of original dataset and labels.
	'''
	print("Sampling subgraphs from dataset \n")
	m = int(ceil((2.0*log(2)*a+log(1.0/delta))/epsilon**2))
	subgraphs = np.ndarray(shape=(m*len(matrices),k,k))
	labels_long = np.ndarray(shape=(len(subgraphs), 1),dtype=int)
	for i,mat in enumerate(matrices):
		for j in range(m):
			subgraphs[i*m+j] = graph_sampler(mat,k)
			labels_long[i*m+j] = labels[i]
	return [subgraphs, labels_long]

def data_preprocessor(graphs, labels, k, nb_classes):
	'''
	Splits data into training and testing set, reshapes
	for compliance with Theano/Keras.
	Returns training and test data and labels.
	'''
	print("Preprocessing dataset")
	X = graphs
	y = labels
	train_fraction = 0.9

	order = arange(len(y))
	shuffle(order)
	X = X[order]
	y = y[order]
	split = int(train_fraction*len(X))
	X_train = X[:split]
	X_test = X[split:]
	y_train = y[:split]
	y_test = y[split:]

	# Reshaping to theano Tensor shape
	X_train = X_train.reshape(X_train.shape[0], 1, k, k)
	X_test = X_test.reshape(X_test.shape[0], 1, k, k)
	X_train = X_train.astype("float32")
	X_test = X_test.astype("float32")

	print('X_train shape:', X_train.shape)
	print(X_train.shape[0], 'train samples')
	print(X_test.shape[0], 'test samples')

	# convert class vectors to binary class matrices
	Y_train = np_utils.to_categorical(y_train, nb_classes)
	Y_test = np_utils.to_categorical(y_test, nb_classes)

	return [X_train, Y_train, X_test, Y_test]


if __name__ == '__main__':
	
	dataset = 'MUTAG'
	
	# Parameters for graph sampling
	k = 5
	a = 34
	delta = 0.05
	epsilon = 0.05
	nb_classes = 2

	# Convnet parameters
	batch_size = 128
	nb_epoch = 12
	loss = 'categorical_crossentropy'
	optimizer = 'adadelta'
	class_mode = 'binary'

	# Getting and preparing data
	matrices, labels = get_data(dataset)
	subgraphs, labels = data_generator(matrices, labels, k, a, epsilon, delta)
	X_train, Y_train, X_test, Y_test = data_preprocessor(subgraphs, labels, k, nb_classes)

	# Building and training model
	model = Model(loss, optimizer, class_mode, nb_classes)
	model.train(X_train, Y_train, X_test, Y_test, batch_size, nb_epoch)
	model.evaluate(X_test, Y_test)