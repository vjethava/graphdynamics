from __future__ import absolute_import

import theano.tensor.signal.conv
from theano.tensor.signal import downsample

import time
import os
import itertools
import scipy
import scipy.io as sio
import numpy as np
import numpy.random as rand
import networkx as nx
import logging
from multiprocessing import Pool
from functools import partial
from keras.utils import np_utils
from keras_models import Model
from graph_samplers import GraphSamplers
from numpy import ceil, log2, log, ones, ravel, reshape, ndarray, arange
from numpy.random import permutation, choice, shuffle
from scipy.special import comb
np.set_printoptions(threshold=np.nan)
#np.random.seed(1337) # for reproducibility


def get_data(dataset, train_frac):
	'''
	Loads .mat dataset containing graphs and labels
	'''

	path = os.path.dirname(os.getcwd())+"/data/"
	if dataset[0]=='N':
		data = sio.loadmat(path+dataset+'.mat')
		labels = data['l'+dataset.lower()].flatten()
		graphs = data[dataset][0]
		adj_matrices = [graph[1] for graph in graphs]
	else:
		data = sio.loadmat(path+dataset+'.mat')
		labels = data['l'+dataset.lower()].flatten()
		graphs = data[dataset][0]
		adj_matrices = [graph[0] for graph in graphs]
	for i in range(len(labels)):
		if labels[i] == 1:
			labels[i] = 1
		else:
			labels[i] = 0

	X = np.asarray(adj_matrices)
	y = np.asarray(labels)
	order = arange(len(y))
	shuffle(order)
	X = X[order]
	y = y[order]
	split = int(train_frac*len(X))
	training_data = X[:split]
	validation_data = X[split:]
	training_labels = y[:split]
	validation_labels = y[split:]

	return [training_data, training_labels, validation_data, validation_labels]


def dataset_shrinker(subgraphs, labels):
	'''
	Reduces the total size of the dataset by removing isomorphic subgraphs
	'''
	print "Shrinking dataset by removing subgraphs that are isomorphic"
	k = subgraphs.shape[1]
	M1, M2 = dict_generator(subgraphs, labels)
	print "Removing isomorphic subgraphs of class 1"
	M1 = remove_isomorphic_subgraphs(M1, k)
	print "Removing isomorphic subgraphs of class 2"
	M2 = remove_isomorphic_subgraphs(M2, k)
	print "Normalizing dataset"
	M1, M2 = dict_scaler(M1, M2)
	class1 = restore_to_ndarray(M1, k)
	class2 = restore_to_ndarray(M2, k)
	labels1 = [0] * len(class1)
	labels2 = [1] * len(class2)
	labels = labels1+labels2
	print "Class 1 contains: "+str(len(M1))+" unique subgraphs."
	print "Class 2 contains: "+str(len(M2))+" unique subgraphs."
	keys_1 = set(M1.keys())
	keys_2 = set(M2.keys())
	intersection = keys_1 & keys_2
	print "A total of "+str(len(intersection))+" of these are common to both classes"
	print "New lenght of dataset: " + str(len(labels))
	print ""
	return [np.concatenate((class1, class2), axis=0), labels]


def dict_generator(subgraphs, labels):

	# Placing graphs in separate dictionaries based on class membership
	M1 = {}
	M2 = {}
	size = len(subgraphs)
	print "Sorting all graphs in ascending order based on number of number of node connections"
	print "Placing them in dictionary 'M[subgraph] = # of subgraphs' for processing."
	for i, graph in enumerate(subgraphs):
		if i % 100000 == 0:
			frac = 100*(i/float(size))
			print "Processed %.2f percent of dataset." % round(frac, 2)
		g = sort_graph_ascending(graph)
		string_rep = np.ndarray.tostring(g)
		if labels[i] == 0:
			if string_rep in M1:
				M1[string_rep] = M1[string_rep] + 1
			else:
				M1[string_rep] = 1
		else:
			if string_rep in M2:
				M2[string_rep] = M2[string_rep] + 1
			else:
				M2[string_rep] = 1
	print "Done!"
	print ""
	return [M1, M2]


def sort_graph_ascending(graph):
    n = graph.shape[0]
    index = range(n)
    count = [sum(row) for row in graph]
    reordering = [x for (y,x) in sorted(zip(count,index), reverse=True)]
    return graph[reordering].T[reordering]


def remove_isomorphic_subgraphs(M, k):
	# Removes a subgraph if isomorphic to other subgraphs in dictionary
	# Combines the counts of isomorphic subgraphs
	size_original = 0
	size_new = 0

	m_keys = M.keys()
	size_m = len(m_keys)
	for i in range(size_m):
		for j in range(i+1,len(m_keys)):
			key1 = m_keys[i]
			key2 = m_keys[j]
			size_original = size_original + M[key1]
			matrix1 = np.fromstring(key1,dtype=bool).reshape((k,k))
			matrix2 = np.fromstring(key2,dtype=bool).reshape((k,k))
			g1 = nx.from_numpy_matrix(matrix1)
			g2 = nx.from_numpy_matrix(matrix2)
			if nx.is_isomorphic(g1, g2) == True:
				M[key1] = M[key1] + M[key2]
				M[key2] = 0
		if i%10 == 0:
			print "%.2f percent done." % round((100*i/float(size_m)),2)

	for key in m_keys:
		if M[key] == 0:
			del M[key]
		else:
			size_new = size_new + M[key]

	frac = (size_new/float(size_original))*100

	print "Reduced number of different subgraphs to %.2f percent size of original" % round(frac, 2)
	print ""
	return M

def dict_scaler(M1, M2):

	'''
	Downscales the size of the dicts and adjusts the ratio of sample such that there are
	1:1 numbers of each class afterwards.
	'''
	print "Downscaling and rebalancing dataset"
	freq1 = sum(M1.values())
	freq2 = sum(M2.values())
	total_size = freq1 + freq2

	if freq1 > freq2:
		ratio = freq1/float(freq2)
		new_total = ratio*freq2 + freq1
		correction = total_size/float(new_total)
		for key in M2.keys():
			M2[key] = int(ratio*correction*0.2*M2[key])
		for key in M1.keys():
			M1[key] = int(correction*0.2*M1[key])
	else:
		ratio = freq2/float(freq1)
		new_total = ratio*freq1 + freq2
		correction = total_size/float(new_total)
		for key in M1.keys():
			M1[key] = int(ratio*correction*0.2*M1[key])
		for key in M2.keys():
			M2[key] = int(correction*0.2*M2[key])

	freq1 = sum(M1.values())
	freq2 = sum(M2.values())
	print "Fraction of original size: ", str((freq1+freq2)/float(total_size))
	print ""

	return [M1, M2]


def restore_to_ndarray(M, k):
	'''
	Restoring the subgraphs from dict format to ndarray.
	'''
	index = 0
	num_class = sum(M.values())
	graphs = np.ndarray(shape=(num_class, k, k), dtype=bool)

	for key in M.keys():
		count = M[key]
		subgraph = np.fromstring(key,dtype=bool).reshape((k,k))
		graphs[index:index+count] = subgraph
		index += count

	return graphs


def data_preprocessor(graphs, labels, k, nb_classes):
	'''
	Splits data into training and testing set, reshapes
	for compliance with Theano/Keras.
	Returns training and test data and labels.
	'''
	print("Preprocessing dataset")
	X = graphs
	y = np.asarray(labels)
	train_fraction = 0.9
	order = arange(len(y))
	shuffle(order)
	X = X[order]
	y = y[order]
	split = int(train_fraction*len(X))
	X_train = X[:split]
	X_test = X[split:]
	y_train = y[:split]
	y_test = y[split:]

	# Reshaping to theano Tensor shape
	X_train = X_train.reshape(X_train.shape[0], 1, k, k)
	X_test = X_test.reshape(X_test.shape[0], 1, k, k)
	X_train = X_train.astype("float32")
	X_test = X_test.astype("float32")

	print('X_train shape:', X_train.shape)
	print(X_train.shape[0], 'train samples')
	print(X_test.shape[0], 'test samples')


	# convert class vectors to binary class matrices
	Y_train = np_utils.to_categorical(y_train, nb_classes)
	Y_test = np_utils.to_categorical(y_test, nb_classes)

	return [X_train, Y_train, X_test, Y_test]


if __name__ == '__main__':

	datasets = ['MUTAG']#, 'NCI109', 'PTC', 'DD', 'NCI1']

	# Parameters for graph sampling
	k = 5
	a = 11
	delta = 0.5
	epsilon = 0.5
	nb_classes = 2
	train_frac = 0.90

	# Convnet parameters
	batch_size = 128
	nb_epoch = 5
	loss = 'categorical_crossentropy'
	optimizer = 'adadelta'
	class_mode = 'binary'

	samplers = GraphSamplers()
	logger = logging.getLogger()


	for data in datasets:
		print('curr_dataset: {}'.format(data))
		# Getting and preparing data
		samplers = GraphSamplers()
		training_data, training_labels, valid_data, valid_labels = get_data(data, train_frac)
		subgraphs, labels_long = samplers.data_generator(training_data, training_labels, k, a, epsilon, delta)
		subgraphs, labels = dataset_shrinker(subgraphs, labels_long)
		X_train, Y_train, X_test, Y_test = data_preprocessor(subgraphs, labels, k, nb_classes)
		input_shape = X_train.shape
		validation_set = samplers.validation_data_generator(valid_data,valid_labels, k, a, epsilon, delta)

		# Building and training model
		model = Model(loss, optimizer, class_mode, nb_classes, input_shape)
		model.train(X_train, Y_train, batch_size, nb_epoch)
		obj_score = model.evaluate(data, validation_set, k)
		print('Finished dataset {} score {}'.format(data, obj_score))
