import scipy.io as sio
import numpy as np
from matplotlib import pyplot as plt

datasets = ['MUTAG.mat','PTC.mat','NCI1.mat','NCI109.mat','DD.mat']


def loadDimensions(name):
	'''
	Function for loading graph datasets
	'''	
	data = sio.loadmat(name)
	name = name[:-4]
	labels = data['l'+name.lower()]
	graphs = data[name][0]

	# Checking if NCIXX dataset, which are formated differently
	if name[0] == 'N':
		dims  = [graph[1].shape[0] for graph in graphs]
		num_classone = sum(1 for p in labels if p == 0)
	else:
		dims = [graph[0].shape[0] for graph in graphs]
		num_classone = sum(1 for p in labels if p == 1)

	class_one = dims[:num_classone]
	class_two = dims[num_classone:]

	return [class_one,class_two]


# Extracting adjacency matrix dimensions and plotting
for i, name in enumerate(datasets):
	x, y = loadDimensions(name)
	x_min = min(min(x),min(y))
	x_max = max(max(x),max(y))

	bins = np.linspace(x_min, x_max, max(len(set(x)),len(set(y))))

	plt.subplot(3, 2, i+1)
	plt.hist(x, bins, alpha=0.5, label="Class 1", color='red')
	plt.hist(y, bins, alpha=0.5, label="Class 2", color='blue')
	plt.title(name[:-4])
	plt.legend(loc='upper right')

plt.show()