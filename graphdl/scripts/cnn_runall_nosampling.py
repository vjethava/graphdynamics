from __future__ import absolute_import
from __future__ import print_function

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.callbacks import Callback
from keras.callbacks import ModelCheckpoint, EarlyStopping

from multiprocessing import Pool
from functools import partial
import time
import os

import scipy.io as sio
import numpy as np
import numpy.random as rand
from numpy import triu, where, ones, ravel, reshape, ndarray, arange, floor
from numpy.random import permutation, choice, shuffle
#np.random.seed(1337) # for reproducibility

class History(Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))

class Model(object):

	def get_model(self, loss, optimizer, class_mode, nb_classes, input_shape):
		'''
		convolutional Neural Network with fully connected output layer
		'''

		input_depth = 1
		nb_filter = 32
		nb_row = 3
		nb_col = 3
		conv_to_dense = nb_filter * (input_shape[2]/2) * (input_shape[2]/2)

		model = Sequential()

		model.add(Convolution2D(nb_filter = nb_filter,
								stack_size = input_depth,
								nb_row = nb_row,
								nb_col = nb_col,
								activation = 'relu',
								border_mode = 'full'))
		model.add(Convolution2D(nb_filter = nb_filter,
								stack_size =nb_filter,
								nb_row = nb_row,
								nb_col = nb_col,
								activation = 'relu'))
		model.add(MaxPooling2D(poolsize = (2, 2))) # Halves the image in each dimension
		model.add(Dropout(0.25))
		model.add(Flatten())
		model.add(Dense(input_dim = conv_to_dense,
						output_dim = 128,
						activation = 'relu'))
		model.add(Dropout(0.5))
		model.add(Dense(input_dim = 128,
						output_dim = nb_classes,
						activation = 'softmax'))

		model.compile(loss=loss, optimizer=optimizer, class_mode=class_mode)
		return model

	def train(self, X_train, Y_train, batch_size, nb_epoch, history, checkpointer):

		self.model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=12,
					show_accuracy=True, verbose=2, callbacks=[history, checkpointer], validation_split=0.1)
		print("History losses")
		print(history.losses)
		print(len(history.losses))


	def evaluate(self, name, X_test, Y_test):
		self.score = self.model.evaluate(X_test, Y_test, show_accuracy=True, verbose=1)
		with open("CNN Unsampled Results.txt", 'a+') as f:
			f.write("Dataset: "+name+"\n")
			f.write("Loss on validation set: "+str(self.score[0])+"\n")
			f.write("Accuracy on validation set: "+str(self.score[1])+"\n")
			f.write("\n")
		
		print('Test Loss: ', self.score[0])
		print('Test Accuracy: ', self.score[1])

	def __init__(self, loss, optimizer, class_mode, nb_classes, input_shape):
		self.loss = loss
		self.optimizer = optimizer
		self.class_mode = class_mode
		self.nb_classes = nb_classes
		self.input_shape = input_shape
		self.model = self.get_model(loss, optimizer, class_mode, nb_classes, input_shape)

def get_data(dataset):
	'''
	Loads .mat dataset containing graphs and labels
	'''

	path = os.path.dirname(os.getcwd())+"/data/"
	if dataset[0]=='N':
		data = sio.loadmat(path+dataset+'.mat')
		labels = data['l'+dataset.lower()]
		graphs = data[dataset][0]
		adj_matrices = [graph[1] for graph in graphs]
	else:
		data = sio.loadmat(path+dataset+'.mat')
		labels = data['l'+dataset.lower()]
		graphs = data[dataset][0]
		adj_matrices = [graph[0] for graph in graphs]
	return [adj_matrices, labels]

def matrixShuffler(matrix):
	'''
	Shuffling rows and columns for data in matrices
	'''
	raveled = ravel(triu(matrix, 1))
	index = where(raveled != 0)
	perm = permutation(raveled[index[0]])
	raveled[index] = perm
	triangular = reshape(raveled, matrix.shape)
	return triangular + triangular.T

def matrix_generator(matrices):

	n = max(mat.shape[0] for mat in matrices)
	adj_matrices = np.ndarray(shape=(len(matrices), n, n), dtype=bool)
	order = np.arange(n)
	rand.shuffle(order)

	for i, mat in enumerate(adj_matrices):
		n_nodes = mat.shape[0]
		adj_matrices[i, 0:n_nodes, 0:n_nodes] = mat
		adj_matrices[i] = matrixShuffler(adj_matrices[i])

	return [adj_matrices, n]

def data_preprocessor(graphs, labels, n, nb_classes):
	'''
	Splits data into training and testing set, reshapes
	for compliance with Theano/Keras.
	Returns training and test data and labels.
	'''
	print("Preprocessing dataset")
	X = graphs
	y = labels
	train_fraction = 0.9

	order = arange(len(y))
	shuffle(order)
	X = X[order]
	y = y[order]
	split = int(train_fraction*len(X))
	X_train = X[:split]
	X_test = X[split:]
	y_train = y[:split]
	y_test = y[split:]

	# Reshaping to theano Tensor shape
	X_train = X_train.reshape(X_train.shape[0], 1, n, n)
	X_test = X_test.reshape(X_test.shape[0], 1, n, n)
	X_train = X_train.astype("float32")
	X_test = X_test.astype("float32")

	print('X_train shape:', X_train.shape)
	print(X_train.shape[0], 'train samples')
	print(X_test.shape[0], 'test samples')

	# convert class vectors to binary class matrices
	Y_train = np_utils.to_categorical(y_train, nb_classes)
	Y_test = np_utils.to_categorical(y_test, nb_classes)

	return [X_train, Y_train, X_test, Y_test]


if __name__ == '__main__':
	
	datasets = ['MUTAG', 'PTC', 'NCI1','NCI109', 'DD']
	
	# Convnet parameters
	batch_size = 64
	nb_epoch = 12
	nb_classes = 2
	loss = 'categorical_crossentropy'
	optimizer = 'adadelta'
	class_mode = 'binary'

	for data in datasets:
		# Getting and preparing data
		print("Running "+data)
		matrices, labels = get_data(data)
		normalized_matrices, n = matrix_generator(matrices)
		X_train, Y_train, X_test, Y_test = data_preprocessor(normalized_matrices, labels, n, nb_classes)
		input_shape = X_train.shape

		# Building and training model
		history = History()
		checkpointer = ModelCheckpoint(filepath="/tmp/weights.hdf5", verbose=1, save_best_only=True)
		model = Model(loss, optimizer, class_mode, nb_classes, input_shape)
		model.train(X_train, Y_train, batch_size, nb_epoch, history, checkpointer)
		model.evaluate(data, X_test, Y_test)