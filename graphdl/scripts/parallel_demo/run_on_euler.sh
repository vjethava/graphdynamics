#!/bin/sh 
#
# See http://deeplearning.net/software/theano/tutorial/multi_cores.html
#
export THEANO_HOME=$HOME/.local/lib64/python2.7/site-packages/theano
export OMP_NUM_THREADS=1;
bsub -n 1 python $THEANO_HOME/misc/check_blas.py
export OMP_NUM_THREADS=16; 
bsub -n 16 python $THEANO_HOME/misc/check_blas.py

