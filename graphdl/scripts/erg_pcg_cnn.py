from __future__ import absolute_import
from __future__ import print_function

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils

import networkx as nx
import numpy as np
import random as rand
np.random.seed(1337) # for reproducibility


'''
Graph Generation Section
'''

# Experiment parameters
l1 = 10000
l2 = 10000
n = 28
p = 0.5
k = int(3*np.sqrt(n))
train_fraction = 0.9

erg = np.ndarray(shape=(l1, n, n), dtype=bool)
pcg = np.ndarray(shape=(l2, n, n), dtype=bool)
nodes = range(n)


def plantClique(mat, nodes, k):
    '''
    Plants clique in Erdos-Renyi graph
    '''
    cliqueMembers = rand.sample(nodes, k)
    
    for i in range(len(cliqueMembers)):
        for j in range(i+1,len(cliqueMembers)):
            cmi = cliqueMembers[i]
            cmj = cliqueMembers[j]
            mat[cmi, cmj] = 1
            mat[cmj, cmi] = 1
    return mat

# Build Erdos Renyi graphs and store them in array
for i in range(l1):
    g = nx.erdos_renyi_graph(n, p)
    mat = nx.to_numpy_matrix(g)
    erg[i] = mat

# Build Planted Clique graphs and store them in array
for i in range(l2):
    g = nx.erdos_renyi_graph(n ,p)
    mat = plantClique(nx.to_numpy_matrix(g), nodes, k)
    pcg[i]= mat

    
# Combining pcg and erg into dataset X and creating class labels y
erg_label = np.zeros(l1)
pcg_label = np.ones(l2)
X = np.concatenate((erg, pcg))
y = np.concatenate((erg_label,pcg_label))

# Shuffling and splitting into testing and training data
order = np.arange(l1+l2)
np.random.shuffle(order)
X = X[order]
y = y[order]
split = int(train_fraction*len(X))
X_train = X[:split]
X_test = X[split:]
y_train = y[:split]
y_test = y[split:]

'''
Preparing and preprocessing the dataset
'''

# Training parameters
batch_size = 128
nb_classes = 2
nb_epoch = 12

# Reshaping and preprocessing
X_train = X_train.reshape(X_train.shape[0], 1, n, n)
X_test = X_test.reshape(X_test.shape[0], 1, n, n)
X_train = X_train.astype("float32")
X_test = X_test.astype("float32")

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)


'''
The Convolutional Neural Network
'''
model = Sequential()

model.add(Convolution2D(32, 1, 3, 3, border_mode='full'))
model.add(Activation('relu'))
model.add(Convolution2D(32, 32, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(poolsize=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(32*196, 128))
model.add(Activation('relu'))
model.add(Dropout(0.5))

model.add(Dense(128, nb_classes))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adadelta')

model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, show_accuracy=True, verbose=1, validation_data=(X_test, Y_test))
score = model.evaluate(X_test, Y_test, show_accuracy=True, verbose=0)
print('Test score:', score[0])
print('Test accuracy:', score[1])