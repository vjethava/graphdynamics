from __future__ import absolute_import
from __future__ import print_function

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils

import scipy.io as sio
import numpy as np
import numpy.random as rand
from numpy import where, triu, ones, ravel, reshape
from numpy.random import permutation
np.random.seed(1337) # for reproducibility

def matrixShuffler(matrix):
	'''
	Shuffling rows and columns for data in matrices
	'''
	raveled = ravel(triu(matrix, 1))
	index = where(raveled != 0)
	perm = permutation(raveled[index[0]])
	raveled[index] = perm
	triangular = reshape(raveled, matrix.shape)
	return triangular + triangular.T

# Data Load
print("Loading MUTAG data \n")
mutag = sio.loadmat('MUTAG.mat')
mkeys = mutag.keys()
labels = mutag['lmutag']
graphs = mutag['MUTAG'][0]

# Building ndArray to hold adjacency matrices
temp_adj_matrices = [graph[0] for graph in graphs]
n = max(mat.shape[0] for mat in temp_adj_matrices)
adj_matrices = np.ndarray(shape = (len(graphs), n, n), dtype=int)

# Adding empty columns and rows to create a matching shape for all adj matrices
for i, matrix in enumerate(temp_adj_matrices):
    n_nodes = matrix.shape[0]
    adj_matrices[i,0:n_nodes, 0:n_nodes] = matrix
print("Built ndarray of %i adjacency matrices." % len(adj_matrices))
print("ndarray shape: %s"  % (adj_matrices.shape, ))
   

# Shuffling all matrices

# CHECK THAT IT CAN BE RECOVERED
order = np.arange(n)
rand.shuffle(order)
for i, matrix in enumerate(adj_matrices):
    adj_matrices[i] = matrixShuffler(matrix)


# Data integrity test
print("Test matrix symmetry for all matrices")
counter = 0
for mat in adj_matrices:
	if (mat != mat.T).all():
		counter += 1
print("No. unsymmetric matrices: %i \n" % counter)

# Formalizing input and output
X = adj_matrices
y = np.ndarray(shape=(len(adj_matrices),1))
print(labels.shape)
y[:][:] = labels
train_fraction = 0.9

# Shuffling and splitting into testing and training data
order = np.arange(len(y))
np.random.shuffle(order)
X = X[order]
y = y[order]
split = int(train_fraction*len(X))
X_train = X[:split]
X_test = X[split:]
y_train = y[:split]
y_test = y[split:]

# Training parameters
batch_size = 128
nb_classes = 2
nb_epoch = 12

# Reshaping and preprocessing
X_train = X_train.reshape(X_train.shape[0], 1, n, n)
X_test = X_test.reshape(X_test.shape[0], 1, n, n)
X_train = X_train.astype("float32")
X_test = X_test.astype("float32")

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)


'''
The Convolutional Neural Network
'''
model = Sequential()

model.add(Convolution2D(32, 1, 3, 3, border_mode='full'))
model.add(Activation('relu'))
model.add(Convolution2D(32, 32, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(poolsize=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(32*196, 128))
model.add(Activation('relu'))
model.add(Dropout(0.5))

model.add(Dense(128, nb_classes))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adadelta')

model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, show_accuracy=True, verbose=1, validation_data=(X_test, Y_test))
score = model.evaluate(X_test, Y_test, show_accuracy=True, verbose=0)
print('Test score:', score[0])
print('Test accuracy:', score[1])