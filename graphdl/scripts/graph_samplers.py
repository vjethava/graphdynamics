import time
import scipy
import itertools
import numpy as np
import numpy.random as rand
from numpy import ceil, log2, log, ones, ravel, reshape, ndarray, arange
from numpy.random import permutation, choice, shuffle
from scipy.special import comb

class GraphSamplers(object):

	def data_generator(self, matrices, labels, k, a, epsilon, delta):
		print "Sampling subgraphs from dataset \n"
		m = int(ceil((2.0*log(2)*a+log(1.0/delta))/epsilon**2))
		n = 0
		for mat in matrices:
			nck = comb(mat.shape[0], k, exact=True)
			if nck > m:
				n = n + m
			else:
				n = n + nck

		subgraphs = np.zeros(shape=(n,k,k), dtype=bool)
		labels_long = np.zeros(0, dtype=bool)
		runtime = 0
		print "Length of subgraphs[]: ", len(subgraphs)
		print "Total matrices: ", len(matrices)
		sampletype = ""
		index = 0
		for i,mat in enumerate(matrices):
			if type(mat) is scipy.sparse.csc.csc_matrix:
				mat = mat.todense()
			else:
				mat = mat
			start = time.clock()
			nck = comb(mat.shape[0], k, exact=True)
			if nck > m:
				sampletype = "sampled"
				subgraphs[index:index+m,:,:] = self.graph_sampler(mat, m, k)
				if labels[i] == 1:
					labels_long = np.append(labels_long, np.ones(m, dtype=np.bool))
				else:
					labels_long = np.append(labels_long ,np.zeros(m, dtype=np.bool))
				index = index + m
			else:
				sampletype = "full"
				subgraphs[index:index+nck,:,:] = self.graph_combinations(mat, k)
				if labels[i] == 1:
					labels_long = np.append(labels_long, np.ones(nck, dtype=np.bool))
				else:
					labels_long = np.append(labels_long ,np.zeros(nck, dtype=np.bool))
				index = index + nck
			runtime = runtime + time.clock() - start
			frac = (i/float(len(labels)))*100
			print "Sampled "+str(i)+ " of "+str(len(matrices))+" matrices."
		print ""
		return [subgraphs, labels_long]

	def validation_data_generator(self, matrices, labels, k, a, epsilon, delta):
		m = int(ceil((2.0*log(2)*a+log(1.0/delta))/epsilon**2))

		validation_data = {}
		runtime = 0
		index = 0
		for i,mat in enumerate(matrices):
			if type(mat) is scipy.sparse.csc.csc_matrix:
				mat = mat.todense()
			else:
				mat = mat
			start = time.clock()
			nck = comb(mat.shape[0], k, exact=True)
			if nck > m:
				validation_data[i] = (self.graph_sampler(mat, m, k), labels[i])
			else:
				validation_data[i] = (self.graph_combinations(mat, k), labels[i])
			runtime = runtime + time.clock() - start
			frac = (i/float(len(labels)))*100
		return validation_data

	def graph_sampler(self, matrix, m, k):
		'''
		Samples k rows and columns without replacement from input matrix.
		Returns subgraph of size k x k.
		'''
		nodes = arange(matrix.shape[0])
		subgraphs = ndarray(shape=(m,k,k), dtype=bool)
		for i in xrange(m):
			order = choice(nodes, k, replace=False)
			subgraphs[i] = matrix[order].T[order]
		return subgraphs

	def graph_combinations(self, matrix, k):
		"Creates all combinations of a given matrix"
		nodes = arange(matrix.shape[0])
		all_combinations = list(itertools.combinations(nodes, k))
		all_combinations = [list(a) for a in all_combinations]
		subgraphs = ndarray(shape=(len(all_combinations), k, k), dtype=bool)
		for i, combo in enumerate(all_combinations):
			subgraphs[i] = matrix[combo].T[combo]
		return subgraphs