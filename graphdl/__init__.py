"""
This package does node classification and graph classification tasks using
convolutional networks
"""

from graphdl import scripts, graphdl, tests
import logging
logger = logging.getLogger('graphdl')
