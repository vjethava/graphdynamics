# Graph Dynamics

This project contains code for graph classification using deep
learning.


## Organization
- mcode: Contains matlab code including Shervashidze code
- graphdl: package containing our implementation	
- tex: Accompanying manuscript

## Comparison with DeepWalk
This section describes how to compare our approach with https://github.com/phanein/deepwalk .
### Datasets
The datasets are available at http://socialcomputing.asu.edu/pages/datasets - download the files
`BlogCatalog3`, `Flickr` and `Youtube2`  from this page. Also
available are updated versions at https://snap.stanford.edu/data/ ; 

### Installation of deepwalk
The best way to install deepwalk is to use `anaconda` available at
http://docs.continuum.io/anaconda/install which can be installed in
your user directory. After installation in home directory, update the
`PATH` as given below

```
export PATH="/home/username/anaconda/bin:$PATH"
```

You can then install `deepwalk` as described on their page
(https://github.com/phanein/deepwalk) 

      cd deepwalk
      pip install -r requirements.txt
      python setup.py install


If there is a `TypeError`, try the following: In the file 

    ~/anaconda/lib/python2.7/site-packages/gensim-0.12.1-py2.7-linux-x86_64.egg/gensim/models/word2vec.py 

replace line 938 which is

    self.syn0[i] = self.seeded_vector(self.index2word[i] + str(self.seed)) 

as follows:

    self.syn0[i] = self.seeded_vector(str(self.index2word[i]) + str(self.seed)) 
    # self.index2word[i] has been converted from int to str to fix TypeError
