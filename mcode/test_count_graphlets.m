% This file counts the number of graphlets 

% graph generation parameters
n = 10;
p = 0.5; 
c = 4; 

% 3-graphlets (for 3-graphlets)
k = 3; 

% approximate kernel sampling parameters 
delta = 0.05; 
epsilon = 0.05; 
a3 = 4; 
a4 = 11; 

get_adj_list = @(A) arrayfun( @(i) find(A(i, :)) , [1 : size(A, 1) ] , 'UniformOutput', false); 

get_prob3_exact = @(A) countall3graphlets(get_adj_list(A)) ./ nchoosek( size(A, 1), 3); 
get_prob4_exact = @(A) countall4graphlets(get_adj_list(A)) ./ nchoosek( size(A, 1), 4); 

A = erdosRenyi(n, p); 
B = getPlantedClique( n, p, c); 

G = struct('al',{}, 'am', {}); 
G(1).al = get_adj_list(A); 
G(1).am = A; 

G(2).al = get_adj_list(B); 
G(2).am = B; 

% exact kernel computation 
K_exact = allkernel(G, k) ;

assert ( K_exact(1, 2) == sum(  get_prob3_exact( G(1).am ) .* get_prob3_exact( G(2).am ) ) ); 

% Approximate kernel computation 
s3 = samplesize(delta, epsilon, a3); 
s4 = samplesize(delta, epsilon, a4); 
if k == 3 
    K_approx = gestkernel3(G, s3); 
elseif k== 4
    K_approx = gestkernel4( G , s4); 
end


%% Exact graphlet counts 
% 1x4 vector of integers. count(i)= number of graphlets with 
% 3-i+1 edges (see 3graphlets.pdf)


%% Approximate graphlet counts 


