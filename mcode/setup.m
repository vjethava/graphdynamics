%
% runs the SVM-THETA code available at http://www.github.com/vjethava/svm-theta
%
SVM_THETA_DIR = '/Users/vjethava/Code/svm-theta/'; 
if exist(SVM_THETA_DIR) 
    run([SVM_THETA_DIR 'setup.m']); 
else
    error('Please install SVM-THETA code from http://www.github.com/vjethava/svm-theta'); 
end

% directory for current code
GD_DIR = pwd; 

% add paths for Shervashidze code 
addpath(genpath([GD_DIR '/graphkernels'])) ; 